from xml_ecg import read_ecg, timestamp_to_datetime

import numpy
from datetime import datetime


def test_read_ecg():
    filename = 'samples/samp1.xml'
    # ecg_dict = xml_to_dict(filename)
    data, info = read_ecg(filename)

    assert data.shape == (12, 10000)
    assert numpy.array_equal(data[:2, :2], numpy.array([[-11, -5], [-2, 1]]))


def test_read_info():
    filename = 'samples/samp1.xml'
    data, info = read_ecg(filename)
    
    expected = {'family_name': u'Nagaraj',
                 'recording_time': datetime(2016, 10, 4, 10, 51, 45),
                 'sex': u'M',
                 'signal_names': ['I', 'II', 'III',
                                  'AVR', 'AVL', 'AVF',
                                  'V1', 'V2', 'V3',
                                  'V4', 'V5', 'V6'],
                'full_signal_names': [u'TIME_ABSOLUTE',
                                      u'MDC_ECG_LEAD_I',
                                      u'MDC_ECG_LEAD_II',
                                      u'MDC_ECG_LEAD_III',
                                      u'MDC_ECG_LEAD_AVR',
                                      u'MDC_ECG_LEAD_AVL',
                                      u'MDC_ECG_LEAD_AVF',
                                      u'MDC_ECG_LEAD_V1',
                                      u'MDC_ECG_LEAD_V2',
                                      u'MDC_ECG_LEAD_V3',
                                      u'MDC_ECG_LEAD_V4',
                                      u'MDC_ECG_LEAD_V5',
                                      u'MDC_ECG_LEAD_V6']
    }


    assert info == expected


def test_timestamp_to_datetime():
    testset = [
        (datetime(2004, 1, 1), '2004'),
        (datetime(2004, 10, 1), '200410'),
        (datetime(2004, 10, 4), '20041004'),
        (datetime(2004, 10, 4, 15, 45, 8), '20041004154508')
    ]
    for exp, tstamp in testset:
        assert exp == timestamp_to_datetime(tstamp)
