import xmltodict
import numpy
from datetime import datetime

from pprint import pprint


# Utilities
def timestamp_to_datetime(timestamp):
    """
    HL7 timestamp has variable resolution
    return datetime object for timestamp
    """
    string_formats = {
        4: "%Y",
        6: "%Y%m",
        8: "%Y%m%d",
        10: "%Y%m%d%H",
        12: "%Y%m%d%H%M",
        14: "%Y%m%d%H%M%S",
        18: "%Y%m%d%H%M%S.%f"   # actually lengths 18-21
        }

    timestamp_length = len(timestamp)
    if timestamp_length < 19:
        string_format = string_formats[len(timestamp)]
    else:
        string_format = string_formats[18]

    return datetime.strptime(timestamp, string_format)


def xml_to_dict(xml_filename):
    """
    Given XML file
    return
    """
    with open(xml_filename) as fd:
        ecg_dict = xmltodict.parse(fd.read())
    return ecg_dict


def read_ecg(ecg_xml):
    """
    Given ECG as path to FDA XML file
    returns
        data as numpy array
        info as dict
    """
    ecg_dict = xml_to_dict(ecg_xml)

    values = _read_data(ecg_dict)
    info = _read_info(ecg_dict)

    # If there are no time values, remove that col
    if values[0] == []:
        values = values[1:]
        info['signal_names'] = info['signal_names'][1:]

    # convert to numpy array
    data = numpy.array(values)

    return data, info


def _read_info(ecg_dict):
    """
    Given ECG dict
    return info as dict

    Implemented values
        - recording_time (datetime obj)
        - family_name
        - sex
        - signal_names (list)
    """
    info = {}

    # "low" and "high" effective times are start and end of recordng
    recording_time = ecg_dict['AnnotatedECG']['effectiveTime']\
                     ['low']['@value']
    info['recording_time'] = timestamp_to_datetime(recording_time)

    # Name and sex
    person = ecg_dict['AnnotatedECG']['componentOf']\
                         ['timepointEvent']['componentOf']\
                          ['subjectAssignment']['subject']['trialSubject']\
                          ['subjectDemographicPerson']

    try:
        info['family_name'] = person['name']['family']['#text']
    except TypeError:  # for biocare this works
        info['family_name'] = person['name']['family']
        
    # info['given_name'] = person['name']['given']['#text']
    info['sex'] = person['administrativeGenderCode']['@code']

    # signal names
    full_signal_names = []
    for seq in ecg_dict['AnnotatedECG']['component']['series']\
        ['component']['sequenceSet']['component']:
        full_signal_names.append(seq['sequence']['code']['@code'])
    info['full_signal_names'] = full_signal_names

    info['signal_names'] = [sn.split('_')[-1] for sn in info['full_signal_names']]

    return info


def _read_data(ecg_dict):
    """
    Given ECG dict
    Return data as numpy array
    """
    values = []

    # extract the lead name and ecg data for each lead
    for seq in ecg_dict['AnnotatedECG']['component']['series']\
        ['component']['sequenceSet']['component']:
        values.append([int(x) for x in seq['sequence']['value'].
                       get('digits', '').split()])

    return values

if __name__ == '__main__':
    sample = 'samples/samp1.xml'
    data, info = read_ecg(sample)
    print info
