Reader for ECG in XML format
============================

Need
----

Written to provide a python script to read data from ECG files stored in FDA XML format from a Mortara ECG machine. 

Implementation
--------------
Reads the XML file 
data returned as numpy array
info returned as python dict

Limitations
-----------
Not all metadata is read currently
Not tested with other XML formats

Testing
-------
Within virtualenv install from requirements.txt   
Run `pytest`
